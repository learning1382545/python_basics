import sys


if __name__ == '__main__':
    print('arg0: ' + sys.argv[0])
    print('arg1: ' + sys.argv[1])
    print('arg2: ' + sys.argv[2])
    print('arg3: ' + sys.argv[3])

# argv[0] es el nombre del programa
# argv[1] es la primer   variable ingresada por teclado
# argv[2] es la segunda  variable ingresada por teclado
# y asi sucesivamente...

# ejemplo de ingreso:
# python sys_argv_example.py primera segunda
