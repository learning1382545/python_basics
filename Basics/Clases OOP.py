# Crear una clase

class Auto:
    def __init__(self, maxima_vel, kilometraje):
        self.maxima_vel = maxima_vel
        self.kilometraje = kilometraje


modelo = Auto(240, 18)
print(modelo.maxima_vel, modelo.kilometraje)

# -----------------------------------------------------------------------------------------------------------------------

# Crear el hijo colectivo que herede lo del padre Auto

class Auto:
    def __init__(self, max_vel, kilometraje):
        self.max_vel = max_vel
        self.kilometraje = kilometraje


class Colectivo(Auto):
    pass


autonuevo = Colectivo(180, 12)
print(" Velocidad: ", autonuevo.max_vel, " Kilometraje: ", autonuevo.kilometraje)

# -----------------------------------------------------------------------------------------------------------------------

# Crear un colectivo que incluya el parametro tarifa y su calculo


class Auto:
    def __init__(self, nombre, max_vel, kilometraje):
        self.nombre = nombre
        self.max_vel = max_vel
        self.kilometraje = kilometraje


class Colectivo(Auto):
    pass

    def calculo(self, tarifa):
        self.tarifa = tarifa


bondi = Colectivo("nombre", 40, 30)
bondi.calculo(40)

print(bondi.nombre, " ", bondi.tarifa)
