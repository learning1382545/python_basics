# Escribir los datos de un archivo .txt en otro, salteandose una linea

# Tambien existe el "a" que es el append: escribe al final del archivo indicado

with open("Ejercicios/Input_and_Output/Ej5primero.txt", "r") as fp :
    lineas = fp.readlines()         # Leo todas las lineas del archivo
    contador = len(lineas)

print("Tiene ", contador, " lineas")
print (lineas)

with open("Ejercicios/Input_and_Output/Ej5segundo.txt", "w") as fp :
    for line in lineas :
        if contador == 5 :
            contador -= 1
            continue
        else :
            fp.write(line)
        contador -= 1

# Chequeo de que un archivo este vacio o no
import os
print(os.stat("Ejercicios/Input_and_Output/Ej6segundo.txt").st_size == 0)
