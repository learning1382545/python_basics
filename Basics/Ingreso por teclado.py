#Ejemplo de ingresar datos por teclado (puede ser una tecla o una palabra entera)
print('')
entrada_teclado = input("Ingrese una tecla: ").lower()
print(entrada_teclado)

#Ejemplo del elif, mezclado con un else para que se vea la diferencia
print('')
if entrada_teclado == 'a' :
    print("Ingresaste a")
elif entrada_teclado == 'b' :
    print("Ingresaste b")
else :
    print("Ingresaste otra cosa")

#Se puede definir la separacion de un muestreo por pantalla

# Mostrar "My name is James" separado por **
print("My", "name", "is", "James", sep="**")

# Aceptar cualquier "three single string" de un solo input()
string1, string2, string3 = input("Ingrese los strings: ").split()

#si en split pusiera ',', por ejemplo, divide por comas
print(string1)
print(string2)
print(string3)