#se muestran if, elif, else

nombre = "Federico"

#Ejemplo del if y del else
if nombre == "Federico" :
    print ("nombre es igual a Federico")
else :
    print ("nombre es distinto a Federico")

if nombre == "Ezequiel" :
    print ("nombre es igual a Ezequiel")
else :
    print ("nombre es distinto a Ezequiel")

#Ejemplo de ingresar datos por teclado (puede ser una tecla o una palabra entera)
print('')
entrada_teclado = input("Ingrese una tecla: ").lower()
print(entrada_teclado)

#Ejemplo del elif, mezclado con un else para que se vea la diferencia
print('')
if entrada_teclado == 'a' :
    print("Ingresaste a")
elif entrada_teclado == 'b' :
    print("Ingresaste b")
else :
    print("Ingresaste otra cosa")

#algo para destacar es que si tengo 3 elif GANA EL PRIMERO QUE SEA TRUE, LUEGO SALE DE LA SECUENCIA DE COMPARACION
#esto es distinto de C, por lo que vale la pena resaltarlo

