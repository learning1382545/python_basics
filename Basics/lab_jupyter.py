import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator

rs = 123

course_genre_url = "https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-ML321EN-SkillsNetwork/" \
                   "labs/datasets/course_genre.csv"
ratings_url = "https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-ML321EN-SkillsNetwork/labs/" \
              "datasets/ratings.csv"

course_df = pd.read_csv(course_genre_url)
ratings_df = pd.read_csv(ratings_url)

# # print(course_df.columns)
#
# print("")
# print("")
# print("")
# print("")
# print("")
#
# # print(course_df.shape[0])
#
# print("")
# print("")
# print("")
# print("")
# print("")
#
# # print(course_df.head())
#
# print("")
# print("")
# print("")
# print("")
# print("")
#
# print(course_df.dtypes)
#
# print("")
# print("")
# print("")
# print("")
# print("")
#
# print(course_df.iloc[1, ])
#
# print("")
# print("")
# print("")
# print("")
# print("")

titles = " ".join(title for title in course_df['TITLE'].astype(str))
# print(titles)
#
# print("")
# print("")
# print("")
# print("")
# print("")

stopwords = set(STOPWORDS)
stopwords.update(["getting started", "using", "enabling", "template", "university", "end", "introduction", "basic"])

wordcloud = WordCloud(stopwords=stopwords, background_color="white", width=800, height=400)
# print(wordcloud.generate(titles))
#
# print("")
# print("")
# print("")
# print("")
# print("")
#
# plt.axis("off")
# plt.figure(figsize=(40, 20))
# plt.tight_layout(pad=0)
# plt.imshow(wordcloud, interpolation='bilinear')
# plt.show()

# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------

course_df['MachineLearning'] == 1
genres = course_df.columns[2:]
print(genres)

# -----------------------------

genre_sums = course_df[genres].sum(axis=0)
pd.DataFrame(genre_sums, columns=['Count'])
genre_sums.sort_values(by="Count", ascending=False)
