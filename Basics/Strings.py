#Aca vemos las diferencias de los strings

comilla_simple = 'Federico Capdeville'
comilla_doble = "Paula Capdeville"

multilinea = """
Parece que aca se puede...

Poner todo en varias lineas...

Veremos para que sirve...
"""

#Para que sirve todo esto? Por si necesito mezclar los tipos de comillas.
#Otra solucion es la siguiente:

#esto permite colocar comillas simples, dobles y todo lo que el ingles o el texto requiera
ejemplo1 = 'You\'ll never do this'
print (ejemplo1)

#esto permite colocar el enter
ejemplo2 = 'Barra n sirve como el enter \nDemostracion'
print (ejemplo2)

#esto permite tabular
ejemplo3 = 'Barra t permite tabular \tEjemplo'
print (ejemplo3)

#esto permite escribir una barra
ejemplo4 = 'Doble barra permite escribir una barra \\Ejemplo'
print (ejemplo4)

#############################################################################################################################################################################################

#manejo de strings por funciones de python (hay muchas mas, tiro ejemplos)
print('')
print(comilla_doble.upper())        #permite poner todas las letras en mayuscula
print(comilla_doble.lower())        #permite poner todas las letras en minuscula
print(comilla_doble.title())        #permite poner las primeras letras en mayuscula y el resto en minuscula

#############################################################################################################################################################################################

#encadenacion de strings
print('')
print(comilla_simple + " hizo este programa")
print("numero " * 3)

#exit permite terminar el programa de forma abrupta
exit(0)
