#aca vamos a ver sentencias condicionales

nombre = "Federico"
edad = 26

#genero booleanos de comparacion por igualdad
print ("Federico" == nombre)
print ("Ezequiel" == nombre)
print ("Ezequiel" != nombre)

#genero booleanos por mayor, menor o igual
print('')
print (60 < edad)
print (60 >= edad)

#operaciones and y or
print('')
print (edad < 100 and edad > 10)
print (edad == 5 or edad == 6)

