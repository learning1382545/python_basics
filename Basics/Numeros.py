#Aca vemos numeros en python

#cosas obvias primero:
#suma                                       +
#resta                                      -
#multiplicacion                             *
#division                                   /
#division (pero solo toma el valor entero)  //
#potencia                                   **

print("Suma: ", 3+2)

print("Resta: ", 3-2)

print("Multiplicacion: ", 3*2)

print("Division: ", 10/2)

print("Potencia: ", 3**2)

#el error que vemos ahora sera por la forma que tiene la computadora para hacer cuentas
print('')
print(0.1+0.2)

#con esto me permite ver el tipo de variable que tengo
print('')
print("Variable: 8 ==> ", type(8))
print('')
print("Variable: 0.1 ==> ", type(0.1))
print('')
print("Variable: lectura ==> ", type('lectura'))

#otra forma de meter variables dentro del print seria...
print('')
print("Meto variables y numeros " + str(9))
