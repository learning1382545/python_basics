import datetime

# Muestra el tiempo
print(datetime.datetime.now())

# Muestra solo el tiempo
print(datetime.datetime.now().time())

#-----------------------------------------------------------------------------------------------------------------------

# Convertir un string en un datetime object

from datetime import datetime

date_string = "Feb 25 2020 4:20PM"

date = datetime.strptime(date_string, '%b %d %Y %I:%M%p')
print(date)

#-----------------------------------------------------------------------------------------------------------------------

# Sustraer una semana a partir de una fecha dada

from datetime import datetime, timedelta

given_date = datetime(2020, 2, 25)

print(given_date - timedelta(days=7))
