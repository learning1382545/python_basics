#aca vamos a ver iteraciones, "for" loops y uso del "while" con python

lista_colores = ["blanco", "rojo", "azul", "negro"]

#se pueden hacer iteraciones dentro de una lista
for color in lista_colores :
    print("color: ", color)

#cuando se termina un for, me queda guardada la variable color y lista para usar
print("Quedo el ultimo color asignado en la variable: ", color);

#############################################################################################################################################################################################

#otro tipo de uso del for
print('')
for i in range(0,3) :
    print(i)

#############################################################################################################################################################################################

#ahora vemos el while
print('')

i = 0
while (i < 5) :
    print("Sigue adentro")
    i = i + 1

print("Quedo el ultimo valor de 'i' asignado en la variable: ", i);

#############################################################################################################################################################################################

# break (escribe HASTA que encuentra el 5, luego sale del for
for i in range(1, 11):
    if i == 5:
        break
    print(i)

#############################################################################################################################################################################################

# continue (escribe HASTA que encuentra el 5, luego sale del if SOLAMENTE en este caso. Luego, vuelve al for
for i in range(1, 8):
    if i == 5:
        continue
    print(i)
