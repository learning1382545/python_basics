#vamos a ver el uso de las listas en python

#defino una lista
numeros = ['uno', 'dos', 'tres']

#formas de usarlo
print (numeros)                 #muestra la lista entera
print (numeros[1])              #muestra segun la posicion
print (numeros[-1])             #del final, trae el -1 (que es el ultimo)

#obtengo desde un elemento en adelante
print (numeros[2:])

#obtengo desde el primero hasta el indicado
print (numeros[:2])

#obtengo el margen indicado
print (numeros[1:2])

#obtener el largo
print(len(numeros))             #largo de una lista
print(len("Federico"))          #largo de un string

#para extender la lista
numeros.append("agrego_final")  #agrego al final de la lista la variable
numeros.extend("fede")          #agrego cada letra como un caracter distinto al final de la lista
print (numeros)

#reemplazar una variable de la lista
numeros[3] = "cuatro"
print('')
print(numeros)

#insertar en la lista en un lugar especifico
numeros.insert(4, "cinco")
print('')
print(numeros)

#borrar de una lista
del numeros[6]                  #borro una posicion especifica
print('')
print(numeros)

numeros.insert(2,'e')
print('')
print(numeros)
numeros.remove('e')             #borro la primera que aparece que coinciden dentro de la lista -- la 'e' final queda
print(numeros)

ultimo = numeros.pop()          #permite borrar el ultimo valor de la lista y (si se quiere) guardarlo por fuera en una variable
print('')
print(ultimo)
print(numeros)

otro = numeros.pop(5)           #permite borrar el indicado dentro del parentesis de la lista
print('')
print(otro)
print(numeros)
