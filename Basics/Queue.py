# https://www.youtube.com/watch?v=5Oe2fbTeKvY&t=2s
# Como trabajar con queue en python

import queue

q = queue.Queue()
q.put(5)
print(list(q.queue))
print(q.empty())
print(q.get())
print(list(q.queue))

# FIFO
print("FIFO")
for x in range(3):
    q.put(x)

print(list(q.queue))
while not q.empty():
    print(q.get(), end="\t")

# -------------------------------------------------------------------------------------------------------------------

print("")
print("---" * 30)
print("LIFO")
# LIFO
q2 = queue.LifoQueue()
for x in range(3):
    q2.put(x)

print(list(q2.queue))
while not q2.empty():
    print(q2.get(), end="\t")

# -------------------------------------------------------------------------------------------------------------------

print("")
print("---" * 30)
print("PRIORITY")

q3 = queue.PriorityQueue()

q3.put((13, "Last element"))
q3.put((3, "Third element"))
q3.put((4, "Fourth element"))
q3.put((2, "Second element"))

for i in range(q3.qsize()):
    print(q3.get())
