# Important: don't call this file 'logging.py'
import logging


def main():
    logging.basicConfig(filename='app.log', level='DEBUG')

    logging.debug("Esto es debug")
    logging.info("Esto es info")
    logging.warning("Esto es warning")
    logging.error("Esto es error")
    logging.critical("Esto es critical")


main()
