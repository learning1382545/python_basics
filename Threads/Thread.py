# https://www.youtube.com/watch?v=7SbpW64wE1k&t=1s
# Uso de thread en python

import threading
import time


def on_hold(arg, time_val):
    print("arg: " + str(arg) + " and time: " + str(time_val))
    time.sleep(time_val)
    print("Thread: " + str(arg) + " finishes its execution")


t = threading.Thread(target=on_hold, name="my_thread", args=("my_thread", 5))
t.start()
print("print number one")

# If I want to wait that the thread finishes its execution

t.join()
print("Printing when the thread finishes")
