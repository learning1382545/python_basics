# https://www.youtube.com/watch?v=-pcCuqH7eh4&t=2s
# Como usar queues en threads


import queue
import threading
import time


def add_item(q):
    while True:
        print("Initiating thread")
        q.put(4)
        time.sleep(2)
        print(list(q.queue))
        print("Execution before the break")
        break


q = queue.Queue()
t = threading.Thread(target=add_item, args=(q, ))

t.start()
q.put(3)
q.join()
print(list(q.queue))
