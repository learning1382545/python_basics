# https://www.youtube.com/watch?v=0Tvq_DRTxb8&t=2s
# Como trabajar con subrutinas

import sys
import os
from pathlib import Path
import subprocess
import subroutine

if __name__ == "__main__":
    global my_process
    my_process = ""
    root = Path(__file__).resolve().parent
    route = os.path.join(root, "subroutine.py")

    my_path = root
    my_process = subprocess.Popen([sys.executable, my_path])
    my_process.communicate()
