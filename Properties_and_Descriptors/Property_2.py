# Example of getter, setter and deleter properties
class Fabric:
    def __init__(self, user):
        self._user = user

    def get_user(self):
        print("Recovering user...")
        return self._user

    def set_user(self, value):
        print("Editing user...")
        self._user = value

    def del_user(self):
        print("Removing user...")
        del self._user

    user = property(get_user, set_user, del_user, "other data")


# Example of inheriting
class Client(Fabric):
    pass


# Declaring a class and printing user through getter
client1 = Client("Anna")
print(client1.user)

# Editing value through setter
client1.user = "Ana"
print(client1.user)

# Removing value through deleter
# del client1.user
# print(client1.user)

# Declaring other possible properties
print(Client.user.__doc__)
