# Example of getter, setter and deleter properties
class AccessInstanceMail:
    def __get__(self, instance, owner):
        return instance._mail+'.ar'

    def __set__(self, instance, value):
        instance._mail = value


class Fabric:
    def __init__(self, user, _mail):
        self._user = user
        self._mail = _mail

    class DescriptorUser:
        """Documentation for descriptor"""

        def __get__(self, instance, owner):
            print("Recovering user...")
            return instance._user.upper()

        def __set__(self, instance, value):
            print("Editing user...")
            instance._user = value

        def __delete__(self, instance):
            print("Removing user...")
            del instance._user

    # Declaring descriptor to use
    user = DescriptorUser()
    mail = AccessInstanceMail()


# Declaring a class and printing user through getter
client1 = Fabric("Anna", "anna@gmail.com")
print(client1.user)
print(client1._mail)
print(client1.mail)

# Editing value through setter
# client1.user = "Ana"
# print(client1.user)

# Removing value through deleter
# del client1.user
# print(client1.user)

# Declaring other possible properties
# print(Client.user.__doc__)
