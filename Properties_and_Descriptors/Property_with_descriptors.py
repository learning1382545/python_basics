# Same example like Property_2.py but with descriptors (more elegant view, same function)
# Example of getter, setter and deleter properties
class DescriptorUser:

    """Documentation for descriptor"""
    def __get__(self, instance, owner):
        print("Recovering user...")
        return instance._user.upper()

    def __set__(self, instance, value):
        print("Editing user...")
        instance._user = value

    def __delete__(self, instance):
        print("Removing user...")
        del instance._user


class Fabric:
    def __init__(self, user):
        self._user = user

    # Declaring descriptor to use
    user = DescriptorUser()


# Declaring a class and printing user through getter
client1 = Fabric("Anna")
print(client1.user)

# Editing value through setter
# client1.user = "Ana"
# print(client1.user)

# Removing value through deleter
# del client1.user
# print(client1.user)

# Declaring other possible properties
# print(Client.user.__doc__)
