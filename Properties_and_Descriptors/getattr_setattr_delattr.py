class People:
    def __init__(self, name):
        self._name = name

    def __getattr__(self, attribute):
        print("Get: " + attribute)
        if attribute == 'name':
            return self._name
        else:
            raise AttributeError(attribute)

    def __setattr__(self, attribute, value):
        print("Set:" + attribute)
        if attribute == 'name':
            attribute = '_name'
        self.__dict__[attribute] = value

    def __delattr__(self, attribute):
        print("Del: " + attribute)
        if attribute == 'name':
            attribute = '_name'
        del self.__dict__[attribute]


people1 = People('Anna')
print(people1.name)

people1.name = "Maria"
del people1.name
