# Same example like Property_2.py but with decorators (more elegant view, same function)
# Example of getter, setter and deleter properties
class Fabric:
    def __init__(self, user):
        self._user = user

    @property
    def user(self):
        """other data"""
        print("Recovering user...")
        return self._user

    @user.setter
    def user(self, value):
        print("Editing user...")
        self._user = value

    @user.deleter
    def user(self):
        print("Removing user...")
        del self._user


# Example of inheriting
class Client(Fabric):
    pass


# Declaring a class and printing user through getter
client1 = Client("Anna")
print(client1.user)

# Editing value through setter
client1.user = "Ana"
print(client1.user)

# Removing value through deleter
# del client1.user
# print(client1.user)

# Declaring other possible properties
print(Client.user.__doc__)
