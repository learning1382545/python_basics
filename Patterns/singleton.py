# https://www.youtube.com/watch?v=ARD5G3QB6Lo&t=2s
# Util cuando necesitamos crear una unica instancia de una determinada clase

# Ver la posicion de memoria... Cada objeto creado nuevo se "pisa" al objeto anterior.
# De forma que solo se utilice uno a la vez

class Usuarios:
    class __Usuarios:
        def __init__(self,):
            self.usuario = None

        def __str__(self):
            return repr(self) + " --- " + self.usuario

        def imprimir(self,):
            print("hola")

    instancia = None

    def __new__(cls):
        if not Usuarios.instancia:
            Usuarios.instancia = Usuarios.__Usuarios()
        return Usuarios.instancia


anna = Usuarios()
anna.usuario = "anita"
print(anna)
print(anna.imprimir())
print("---"*23)
pedro = Usuarios()
pedro.usuario = "pedrito"
print(pedro)
print(pedro.imprimir())
