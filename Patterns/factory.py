# https://www.youtube.com/watch?v=saLiPsCJNPA&t=1s
# A partir de una funcion, crea diferentes instancias de diferentes clases

def factory(clase, *args, **kwargs):
    return clase(*args, **kwargs)


class Auto:
    def frenar(self, arg):
        print(arg)


class Moto:
    def __init__(self, color, marca="Fiat"):
        self.color = color
        self.marca = marca


obj1 = factory(Auto)
obj1.frenar("El valor es...")

obj2 = factory(Moto, "Azul", "Chevrolet")
print(obj2.color)
