# https://www.youtube.com/watch?v=zuQVlLu_2iM&t=1s

class Subject:
    observador = []

    def agregar(self, obj):
        self.observador.append(obj)

    def quitar(self, obj):
        pass

    def notificar(self):
        for in_observador in self.observador:
            in_observador.update()


class TemaConcreto(Subject):
    def __init__(self, ):
        self.estado = None

    def set_estado(self, value):
        self.estado = value
        self.notificar()

    def get_estado(self, ):
        return self.estado


class Observador:
    def update(self, ):
        raise NotImplementedError("Delegación de actualización")


class ConcreteObserverA(Observador):
    def __init__(self, obj):
        self.observado_a = obj
        self.observado_a.agregar(self)

    def update(self, ):
        print("Actualización dentro de Observador ConcreteObserverA")
        self.estado = self.observado_a.get_estado()
        print("Estado: " + str(self.estado))


class ConcreteObserverB(Observador):
    def __init__(self, obj):
        self.observado_b = obj
        self.observado_b.agregar(self)

    def update(self, ):
        print("Actualización dentro de Observador ConcreteObserverB")
        self.estado = self.observado_b.get_estado()
        print("Estado: " + str(self.estado))


tema1 = TemaConcreto()
observador_a = ConcreteObserverA(tema1)
observador_b = ConcreteObserverB(tema1)
tema1.set_estado(1)
