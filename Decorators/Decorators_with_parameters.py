def modo_de_trabajo(valor=False):
    def _modo_de_trabajo(funcion):
        def interna(*args, **kwargs):
            print(*args)
            print("")
            if valor:
                print("Estoy en producción")
            else:
                print("Estoy en desarrollo")

            for id, argumento in enumerate(args):
                print("n: " + str(id) + " - argumento: " + argumento)
            funcion(*args, **kwargs)
        return interna
    return _modo_de_trabajo


@modo_de_trabajo(False)
def registro_usuario(nombre, apellido):
    print("Registro de usuario: " + nombre + " " + apellido)


registro_usuario("Ana", "Rodriguez")
