# Add functionality to an existing code.
# Also called metaprogramming as a part of the program tries to modify another part of the program at compile time.

# Demonstration
def my_smart_div(func):
    def inner_func(x, y):
        print("I am dividing ", x, " and ", y)
        if y == 0:
            print("Oops! Division by zero is illegal...!!!")
            return

        return func(x, y)
    return inner_func


@my_smart_div
def go_divide(a, b):                # Generally, we decorate a function and reassign it
    return a/b                      # go_divide = my_smart_div(go_divide


print(go_divide(20, 2))
print()
print(go_divide(20, 0))
