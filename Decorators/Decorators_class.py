class DecoradorMultiplicarPor10:
    def __init__(self, func):
        self.func = func

    def __call__(self, *args, **kwargs):
        print(self.func(*args)*10)


@DecoradorMultiplicarPor10
def sumar(a, b):
    c = a + b
    return c


sumar(2, 3)
